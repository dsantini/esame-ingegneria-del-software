﻿using System;
using System.Drawing;

namespace test
{
	public interface ImageComparator
	{
		bool compareImage (Image a, Image b);
	}
}

