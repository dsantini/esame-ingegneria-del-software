# 📝 Proposta 📝
# ⚠ Scartata ⚠

# Sistema di sorveglianza per la casa
- Attreverso una telecamera scatta delle foto ogni volta che rileva un movimento
  - Prova a riconoscere e identifica le facce di chi passa davanti alla telecamera.
  - In base a chi viene identificato (o non identifcato) viene emesso un mesaggio diverso.
    - Il messaggio può parlare di meteo o di  altre informazioni.
  - I fotogrammi scattati vengono salvati permanentemente
- Permette di vedere tutti i fotogrammi scattati e aggiungere nuove faccie conosciute
  - Client (interfaccia utente) che si collega al server (il modulo con la telecamera)

## Altre possibilità
- Esiste un amministratore (riconosciuto con la faccia) che all'arrivo viene avvisato dell'ingresso di sconosciuti
- inviare una mail al proprietario se viene rilevata una persona sconosciuta

## Hardware
- Raspberry Pi 3 ($39.95): https://www.adafruit.com/products/3055
- Telecamera ($29.95): https://www.adafruit.com/products/3099
- Case per la telecamera ($2.95): https://www.adafruit.com/products/3253

Prezzo totale: circa 70€, circa 23€ a testa

## Software
- C# (imposto)
- EmguCV per il riconoscimento dei volti: http://www.emgu.com/wiki/index.php/Main_Page
- Text To Speech: https://msdn.microsoft.com/it-it/library/system.speech.synthesis(v=vs.110).aspx

#### Possiblità per C Sharp
- mono su Linux (ma non funziona TTS, quindi non va bene)
- .NET su Windows-IoT

