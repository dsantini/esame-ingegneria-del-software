| Termine | Significato |
| ----- | ----- |
| Amministratore | Utente che ha l'autorizzazione per visualizzare, aggiungere e eliminare tutti i dati (camion, destinazioni, percorsi, persone, tipi di rifiuto, punti di raccolta, turni). Un amministratore pu� promuovere un utente a amministratore o demansionare un amministratore (incluso se stesso, a condizione che non sia l�unico) a operatore. |
| Autenticazione / Login | Operazione che prevede l'inserimento e verifica di codice fiscale e password di una persona. DEVE essere eseguito all'inizio di ogni sessione. Permette di essere identificarlo come amministratore o utente operatore. |
| Camion | Veicolo in grado di raccogliere e trasportare rifiuti. Caratterizzato dai tipi di rifiuto che pu� raccogliere e dalla targa univoca. |
| Destinazione | Struttura in cui un camion pu� sversare i rifiuti che ha raccolto (per esempio discarica, inceneritore, ...). Caratterizzata dal nome e dall'indirizzo, univoco. |
| Disconnessione / Logout | Termina una sessione utente. Dopo di essa non si possono pi� eseguire operazioni fino a una nuova autenticazione. Deve essere effettuata dall'utente o automaticamente all'uscita dal programma. |
| Equipaggio | Insieme di persone che gestiranno la raccolta in un viaggio. |
| Itinerario | Elenco ordinato di punti di raccolta che un camion seguir� in un viaggio. |
| Operatore | Netturbino che guida un camion e/o gestisce il caricamento dei rifiuti su di esso. Utente in grado, una volta autenticato, di visualizzare i dettagli dei viaggi previsti di cui fara' parte. Pu� essere promosso a amministratore da un amministratore.  |
| Percorso | Tragitto seguito da un camion durante un viaggio. Composto da un itinerario e una destinazione. |
| Persona | Dipendente. Operatore o amministratore. Caratterizzata da nome, cognome e codice fiscale univoco. Pu� accedere al sistema autenticandosi con codice fiscale e password. |
| Punto di raccolta | Stazione in cui ogni cittadino pu� portare i propri rifiuti e che i netturbini provvederanno a svuotare. Caratterizzato dal nome, univoco, e dalla via in cui si trova. |
| Rifiuto | Materiale che l'azienda si occupa di prelevare e smaltire. |
| Tipo di rifiuto | Categoria di rifiuto (per esempio indifferenziato, vetro, ...). Identificato dal nome. |
| Turno | Insieme di viaggi caratterizzati dallo stesso percorso, tipo di rifiuto, camion e equipaggio. |
| Turno Giornaliero | Turno che prevede la partenza ogni giorno nello stesso orario in un intervallo di date. |
| Turno settimanale | Turno che prevede la partenza una volta alla settimana in un certo intervallo di date, sempre nello stesso giorno della settimana e alla stessa ora. |
| Turno una tantum | Turno che prevede una sola partenza. |
