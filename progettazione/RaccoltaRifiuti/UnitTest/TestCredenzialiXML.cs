﻿using NUnit.Framework;
using RaccoltaRifiuti.Model;
using RaccoltaRifiuti.Persistenza;
using RaccoltaRifiuti.Persistenza.XML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest
{
    [TestFixture]
    public class TestCredenzialiXML
    {

        [Test]
        public void VerificaCredenziali()
        {
            ICredenziali c = new CredenzialiXML("../../TestCredenzialiXML.xml", true);
            Assert.IsTrue(c.VerificaCredenziali("EDC", "wsx"));
            Assert.IsFalse(c.VerificaCredenziali("edc", "xyz"));
            Assert.IsTrue(c.VerificaCredenziali("yhn", "ijn"));
        }

        [Test]
        public void ModificaCredenziali()
        {
            ICredenziali c = new CredenzialiXML("modifica.xml", false);
            Assert.IsFalse(c.VerificaCredenziali("paperino", "password"));
            c.ModificaCredenziali("paperino", "password");
            Assert.IsTrue(c.VerificaCredenziali("paperino", "password"));
        }

        [Test]
        public void RimuoviCredenziali()
        {
            ICredenziali c = new CredenzialiXML("rimuovi.xml", false);
            c.ModificaCredenziali("paperino", "password");
            c.RimuoviCredenziali("paperino");
            Assert.IsFalse(c.VerificaCredenziali("paperino", "password"));
        }
    }
}
