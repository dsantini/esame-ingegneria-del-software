﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RaccoltaRifiuti.Persistenza;
using RaccoltaRifiuti.Persistenza.XML;
using RaccoltaRifiuti.Presenter.Dati;
using RaccoltaRifiuti.Model;

namespace UnitTest
{
    [TestFixture]
    class TestGestioneModello
    {
        GestioneModello _gestione;

        [SetUp]
        public void SetUp()
        {
            _gestione = new GestioneModello(new PersistenzaXML("pippo.xml", false));
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PersistenzaNulla()
        {
            new GestioneModello(null);
        }

        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public void AggiungiDestinazioneDuplicata()
        {
            _gestione.AggiungiDestinazione("pippo", "pluto");
            _gestione.AggiungiDestinazione("pippo", "pluto");
        }

        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public void AggiungiPersonaDuplicata()
        {
            _gestione.AggiungiPersona("abc123", "a", "b");
            _gestione.AggiungiPersona("abc123", "a", "b");
        }

        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public void AggiungiPuntoDiRaccoltaDuplicato()
        {
            _gestione.AggiungiPuntoDiRaccolta("pippo", "pluto");
            _gestione.AggiungiPuntoDiRaccolta("pippo", "pluto");
        }

        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public void AggiungiTipoRifiutoDuplicato()
        {
            _gestione.AggiungiTipoDiRifiuto("umido");
            _gestione.AggiungiTipoDiRifiuto("umido");
        }
    }
}