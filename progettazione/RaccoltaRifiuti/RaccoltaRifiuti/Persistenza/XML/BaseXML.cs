﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace RaccoltaRifiuti.Persistenza.XML
{
    public abstract class BaseXML
    {
        protected readonly XmlDocument _doc;
        protected readonly string _fileName;

        public BaseXML(String fileName, bool append)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                throw new ArgumentException("Nome file vuoto");

            _fileName = fileName;
            _doc = new XmlDocument();
            if (append)
            {
                if (!File.Exists(fileName))
                    throw new FileNotFoundException(fileName);

                try
                {
                    _doc.Load(fileName);
                    //_doc.Load(XmlReader.Create(fileName, new XmlReaderSettings() { ValidationType=ValidationType.Schema, ValidationFlags=System.Xml.Schema.XmlSchemaValidationFlags.ProcessSchemaLocation }));
                }
                catch (Exception e)
                {
                    throw new ApplicationException("Impossibile caricare " + fileName + " : " + e.Message, e);
                }
            }
        }
    }
}
