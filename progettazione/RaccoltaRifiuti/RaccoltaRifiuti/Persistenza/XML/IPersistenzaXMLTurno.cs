﻿using RaccoltaRifiuti.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace RaccoltaRifiuti.Persistenza.XML
{
    public interface IPersistenzaXMLTurno
    {
        Type TipoAccettato { get; }
        IEnumerable<Turno> Carica(XmlDocument doc, IEnumerable<Persona> persone, IEnumerable<TipoRifiuto> tipi, IEnumerable<Camion> camion, IEnumerable<Percorso> Percorsi);
        bool Salva(XmlDocument doc, Turno t, string nomeFile);
    }
}
