﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using RaccoltaRifiuti.Model;
using RaccoltaRifiuti.Utils;

namespace RaccoltaRifiuti.Persistenza.XML.Turni
{
    public class XMLTurno
    {
        public static IEnumerable<Persona> GetEquipaggio(XmlElement turno, IEnumerable<Persona> persone)
        {
            return from XmlElement sottoNodo
                   in turno.SelectNodes("Partecipante[@codiceFiscale]")
                   let codiceFiscale = sottoNodo.GetAttribute("codiceFiscale").Ripulisci()
                   join Persona persona in persone on codiceFiscale equals persona.CodiceFiscale
                   select persona;
        }

        public static XmlElement CreaElementoTurno(XmlDocument doc, string nomeTag, int id, out XmlElement insiemeTurni)
        {
            insiemeTurni = doc.OttieniInsieme("InsiemeTurni");
            XmlElement e = (XmlElement)doc.SelectSingleNode("/Model/InsiemeTurni/" + nomeTag + "[@id = '" + id + "']");

            if (e != null)
                e.RemoveAll();
            else // L'elemento per il turno con questo id non esiste ancora
            {
                e = doc.CreateElement(nomeTag);
                insiemeTurni.AppendChild(e);
            }

            return e;
        }

        public static void PreparaAttributiTurno(XmlDocument doc, XmlElement turno, Turno t)
        {
            turno.SetAttribute("id", t.ID.ToString());
            turno.SetAttribute("tipo", t.Tipo.Nome);
            turno.SetAttribute("Percorso", t.PercorsoEffuttuato.Identificativo.ToString());
            turno.SetAttribute("camion", t.CamionUsato.Targa);
            foreach (Persona pers in t.Equipaggio)
            {
                XmlElement part = doc.CreateElement("Partecipante");
                part.SetAttribute("codiceFiscale", pers.CodiceFiscale);
                turno.AppendChild(part);
            }
        }
    }
}
