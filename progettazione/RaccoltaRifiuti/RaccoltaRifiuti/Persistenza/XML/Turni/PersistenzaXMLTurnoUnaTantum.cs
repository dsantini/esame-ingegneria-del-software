﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using RaccoltaRifiuti.Model;
using RaccoltaRifiuti.Model.Turni;
using System.Globalization;
using RaccoltaRifiuti.Utils;

namespace RaccoltaRifiuti.Persistenza.XML.Turni
{
    public class PersistenzaXMLTurnoUnaTantum : IPersistenzaXMLTurno
    {
        public Type TipoAccettato { get { return typeof(TurnoUnaTantum); } }

        public IEnumerable<Turno> Carica(XmlDocument doc, IEnumerable<Persona> persone, IEnumerable<TipoRifiuto> tipi, IEnumerable<Camion> camion, IEnumerable<Percorso> Percorsi)
        {
            return from XmlElement nodo
                   in doc.SelectNodes("/Model/InsiemeTurni/TurnoUnaTantum[@id][@dataOraInizio][@tipo][@Percorso][@camion][Partecipante[@codiceFiscale]]")
                   let id = int.Parse(nodo.GetAttribute("id"))
                   let nomeTipo = nodo.GetAttribute("tipo")
                   let idPercorso = int.Parse(nodo.GetAttribute("Percorso"))
                   let targaCamion = nodo.GetAttribute("camion").Ripulisci()
                   let dataOraInizio = DateTime.Parse(nodo.GetAttribute("dataOraInizio"), DateTimeFormatInfo.InvariantInfo)
                   join TipoRifiuto tipo in tipi on nomeTipo equals tipo.Nome
                   join Camion camionUtilizzato in camion on targaCamion equals camionUtilizzato.Targa
                   where camionUtilizzato.TipiSupportati.Contains(tipo)
                   join Percorso Percorso in Percorsi on idPercorso equals Percorso.Identificativo
                   let equipaggio = XMLTurno.GetEquipaggio(nodo, persone)
                   select new TurnoUnaTantum(dataOraInizio, id, Percorso, equipaggio.ToList(), camionUtilizzato, tipo);
        }

        public bool Salva(XmlDocument doc, Turno t, string nomeFile)
        {
            bool result;

            if (t == null || !(t is TurnoUnaTantum))
                result = false; // Non valido
            else
            {
                TurnoUnaTantum tut = (TurnoUnaTantum)t;
                XmlElement insiemeTurni, e = XMLTurno.CreaElementoTurno(doc, "TurnoUnaTantum", tut.ID, out insiemeTurni);

                try
                {
                    XMLTurno.PreparaAttributiTurno(doc, e, tut);
                    e.SetAttribute("dataOraInizio", tut.Inizio.ToString("s"));
                    doc.Save(nomeFile);
                    result = true;
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Impossibile salvare il turno " + tut.ID + ": " + ex.Message);
                    insiemeTurni.RemoveChild(e);
                    result = false;
                }
            }

            return result;
        }
    }
}
