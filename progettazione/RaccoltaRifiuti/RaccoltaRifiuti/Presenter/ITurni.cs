﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RaccoltaRifiuti.Model;
using System.ComponentModel;

namespace RaccoltaRifiuti.Presenter
{
    public interface ITurni
    {
        BindingList<Camion> Camion { get; }
        BindingList<Persona> Persone { get; }
        BindingList<TipoRifiuto> TipiDiRifiuto { get; }
        BindingList<Percorso> Percorsi { get; }
        BindingList<Turno> Turni { get; }

        int NuovoIdTurno();
        void AggiungiTurno(Turno t);
    }
}
