﻿using RaccoltaRifiuti.Model;
using RaccoltaRifiuti.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.Presenter.WindowsForms
{
    class GestionePercorsi
    {
        private IPercorsiView _gvf;
        private IPercorsi _gestmodello;

        private IEnumerable<Percorso> PercorsiSelezionati { get { return from DataGridViewRow riga in _gvf.DataView.SelectedRows select (Percorso)riga.DataBoundItem; } }
        private TimeSpan DurataPrevista { get { return _gvf.DurataPrevista.Value.TimeOfDay; } }

        public GestionePercorsi(IPercorsiView form, IPercorsi modello)
        {
            if (form == null)
                throw new ArgumentNullException("form");
            if (modello == null)
                throw new ArgumentNullException("modello");

            _gvf = form;
            _gestmodello = modello;

            // Aggancia gli handler degli input
            form.Aggiungi.Click += Aggiungi_Click;
            form.Rimuovi.Click += Rimuovi_Click;
            form.Destinazione.SelectedIndexChanged += SelectedIndexChanged;
            form.PuntiDiRaccolta.MouseUp += SelectedIndexChanged;
            form.DurataPrevista.ValueChanged += SelectedIndexChanged;

            // Prepara la DataGridView
            form.DataView.SelectionChanged += DataView_SelectionChanged;
            form.DataView.DataSource = modello.Percorsi;
            form.DataView.CellFormatting += (s, e) => { e.Value = (e.Value is List<PuntoDiRaccolta>) ? string.Join(Environment.NewLine, from p in (List<PuntoDiRaccolta>)e.Value select p.Nome) : e.Value.ToString(); };

            // Prepara le ComboBox e le CheckedListBox
            foreach (Destinazione d in _gestmodello.Destinazioni)
                _gvf.Destinazione.Items.Add(d);
            foreach (PuntoDiRaccolta p in _gestmodello.PuntiDiRaccolta)
                _gvf.PuntiDiRaccolta.Items.Add(p);
        }



        private void DataView_SelectionChanged(object sender, EventArgs e)
        {
            _gvf.Rimuovi.Enabled = _gvf.DataView.SelectedRows.Count > 0;
        }

        private void SelectedIndexChanged(object sender, EventArgs e)
        {
            _gvf.Aggiungi.Enabled = _gvf.Destinazione.SelectedItem != null && DurataPrevista > TimeSpan.Zero && _gvf.PuntiDiRaccolta.CheckedItems.Count > 0;
        }

        private void Rimuovi_Click(object sender, EventArgs e)
        {
            foreach (Percorso v in PercorsiSelezionati)
            {
                _gestmodello.Percorsi.Remove(v);
            }
        }

        private void Aggiungi_Click(object sender, EventArgs e)
        {
            try
            {
                _gestmodello.AggiungiPercorso(DurataPrevista, (Destinazione)_gvf.Destinazione.SelectedItem, _gvf.PuntiDiRaccolta.CheckedItems.OfType<PuntoDiRaccolta>().ToList());
            }
            catch (Exception exc)
            {
                MessageBox.Show("Parametri non validi: " + exc.Message, "Errore nell'inserimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
