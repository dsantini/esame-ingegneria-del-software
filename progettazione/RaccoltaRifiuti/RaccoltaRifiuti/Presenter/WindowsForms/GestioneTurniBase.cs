﻿using RaccoltaRifiuti.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaccoltaRifiuti.Presenter.WindowsForms
{
    public abstract class GestioneTurniBase
    {
        protected ITurniView View { get; private set; }
        protected ITurni Mod { get; private set; }

        public GestioneTurniBase(ITurniView gest, ITurni mod)
        {
            if (gest == null)
                throw new ArgumentNullException("view");
            if (mod == null)
                throw new ArgumentNullException("model");

            View = gest;
            Mod = mod;
        }
    }
}
