﻿using RaccoltaRifiuti.View;
using RaccoltaRifiuti.View.WindowsForms;
using RaccoltaRifiuti.Persistenza;
using RaccoltaRifiuti.Presenter.Dati;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Windows.Forms;
using RaccoltaRifiuti.Persistenza.XML;
using RaccoltaRifiuti.Model;
using System.ComponentModel;
using RaccoltaRifiuti.Presenter.WindowsForms.Turni;

namespace RaccoltaRifiuti.Presenter.WindowsForms
{
    class MainPresenter
    {
        private StatoUtente _stato;
        private IModello _gestoreModello;
        private ICredenziali _credenziali;
        private List<Form> _forms;

        private IModello Modello
        {
            get { return _gestoreModello; }
            set
            {
                if (value != _gestoreModello)
                {
                    if (_gestoreModello != null)
                    {   // Sgancia gli handler per il cambiamento delle liste dal vecchio modello
                        ((ICamions)_gestoreModello).Camion.ListChanged -= ListChanged;
                        ((IDestinazioni)_gestoreModello).Destinazioni.ListChanged -= ListChanged;
                        ((IPercorsi)_gestoreModello).Percorsi.ListChanged -= ListChanged;
                        ((IPersone)_gestoreModello).Persone.ListChanged -= ListChanged;
                        ((IPuntiDiRaccolta)_gestoreModello).PuntiDiRaccolta.ListChanged -= ListChanged;
                        ((ITipiDiRifiuto)_gestoreModello).TipiDiRifiuto.ListChanged -= ListChanged;
                        _gestoreModello.Turni.ListChanged -= ListChanged;
                    }

                    _gestoreModello = value;
                    ListChanged(null, null);

                    if (_gestoreModello != null)
                    {   // Aggancia gli handler per il cambiamento delle liste
                        ((ICamions)_gestoreModello).Camion.ListChanged += ListChanged;
                        ((IDestinazioni)_gestoreModello).Destinazioni.ListChanged += ListChanged;
                        ((IPercorsi)_gestoreModello).Percorsi.ListChanged += ListChanged;
                        ((IPersone)_gestoreModello).Persone.ListChanged += ListChanged;
                        ((IPuntiDiRaccolta)_gestoreModello).PuntiDiRaccolta.ListChanged += ListChanged;
                        ((ITipiDiRifiuto)_gestoreModello).TipiDiRifiuto.ListChanged += ListChanged;
                        _gestoreModello.Turni.ListChanged += ListChanged;
                    }
                }
            }
        }


        public MainPresenter(ILoginView lv, IMainView mv, IPersistenza p, ICredenziali c)
        {

            if (lv == null)
                throw new ArgumentNullException("login form null");
            if (mv == null)
                throw new ArgumentException("main form null");
            if (p == null)
                throw new ArgumentException("persistenza null");

            _credenziali = c;

            // Prepara le liste per la gestione dati
            _forms = new List<Form>();
            new LoginPresenter(Login, lv, _credenziali, () => { return ((IPersone)Modello).Persone; });
            _stato = new Sconosciuto(mv, lv);
            Modello = new GestioneModello(p);

            // Aggancia gli handler dei ToolStripMenuItem
            mv.Salva.Click += Salva_Click;
            mv.Camion.Click += Camion_Click;
            mv.Persone.Click += Persone_Click;
            mv.TipiDiRifiuto.Click += TipiDiRifiuto_Click;
            mv.Destinazioni.Click += Destinazioni_Click;
            mv.PuntiDiRaccolta.Click += PuntiDiRaccolta_Click;
            mv.Percorsi.Click += Percorsi_Click;
            mv.Esci.Click += Esci_Click;
            mv.Logout.Click += Logout_Click;
            mv.Credenziali.Click += Credenziali_Click;

            mv.FormClosed += Esci_Click;

#if DEBUG // "Salva su" e "Carica da" funzionano solo in debug
            mv.SalvaSu.Click += SalvaSu_Click;
            mv.CaricaDa.Click += CaricaDa_Click;
#else
            mv.CaricaDa.Visible = false;
            mv.SalvaSu.Visible = false;
#endif
            CaricaToolStripMenuItemGestioneTurni(mv.Gestione);
        }

        private void CaricaToolStripMenuItemGestioneTurni(ToolStripMenuItem radice)
        {
            // Carica i ToolStripMenuItem per la gestione dei turni
            foreach (Type t in Assembly.GetExecutingAssembly().GetTypes())
            {
                if (!t.IsAbstract && typeof(GestioneTurniBase).IsAssignableFrom(t))
                {
                    ToolStripMenuItem tsmi = new ToolStripMenuItem();

                    // Nome 
                    MethodInfo titolo = t.GetProperty("Titolo").GetMethod;
                    tsmi.Text = titolo != null ? (string)titolo.Invoke(null, null) : t.Name.Substring(8); // GestoreTurnoGiornaliero => TurnoGiornaliero

                    // Azione al click
                    tsmi.Click += ((sender, args) =>
                    {
                        GestioneTurnoForm gtf = new GestioneTurnoForm();
                        GestioneTurniBase g = (GestioneTurniBase)t.GetConstructor(new Type[] { typeof(ITurniView), typeof(ITurni) }).Invoke(new object[] { gtf, Modello });
                        _forms.Add(gtf);
                        gtf.Show();
                    });
                    radice.DropDownItems.Add(tsmi);
                }
            }
        }

        private void NascondiFigli()
        {
            while (_forms.Count > 0)
            {
                _forms.ElementAt(0).Close();
                _forms.RemoveAt(0);
            }
        }

        private void Logout_Click(object sender, EventArgs e)
        {

            NascondiFigli();
            _stato = new Sconosciuto(_stato.Main, _stato.Login);
        }

        public void Login(Persona utente)
        {
            _stato = new Autenticato(utente, _stato.Main, _stato.Login, () => { return Modello.Turni; });
        }

        private void Esci_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void CaricaDa_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Filter = "File XML|*.xml";
                if (ofd.ShowDialog() == DialogResult.OK)
                    SetPersistenzaXML(ofd.FileName);
            }
        }

        private void SetPersistenzaXML(string fileName)
        {
            NascondiFigli();

            try
            {
                PersistenzaXML p = new PersistenzaXML(fileName, true);
                Modello = new GestioneModello(p);
                Logout_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore durante il caricamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SalvaSu_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = "File XML|*.xml";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        Modello.Salva(new PersistenzaXML(sfd.FileName, false));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Errore durante il salvataggio", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void Credenziali_Click(object sender, EventArgs e)
        {
            GestioneCredenzialiForm gcf = new GestioneCredenzialiForm();
            new GestioneCredenziali(gcf, _credenziali, (from p in ((IPersone)_gestoreModello).Persone select p.CodiceFiscale).ToList());
            _forms.Add(gcf);
            gcf.Show();
        }

        private void Percorsi_Click(object sender, EventArgs e)
        {
            GestionePercorsiForm gvf = new GestionePercorsiForm();
            new GestionePercorsi(gvf, Modello);
            _forms.Add(gvf);
            gvf.Show();
        }

        private void PuntiDiRaccolta_Click(object sender, EventArgs e)
        {
            GestioneIndirizzoForm gdf = new GestioneIndirizzoForm();
            new GestionePuntiDiRaccolta(gdf, Modello);
            _forms.Add(gdf);
            gdf.Show();
        }

        private void Destinazioni_Click(object sender, EventArgs e)
        {
            GestioneIndirizzoForm gdf = new GestioneIndirizzoForm();
            new GestioneDestinazioni(gdf, Modello);
            _forms.Add(gdf);
            gdf.Show();
        }

        void TipiDiRifiuto_Click(object sender, EventArgs e)
        {
            GestioneTipiForm gtf = new GestioneTipiForm();
            new GestioneTipi(gtf, Modello);
            _forms.Add(gtf);
            gtf.Show();
        }


        void Camion_Click(object sender, EventArgs e)
        {
            GestioneCamionForm guf = new GestioneCamionForm();
            new GestioneCamion(guf, Modello);
            _forms.Add(guf);
            guf.Show();
        }


        void Persone_Click(object sender, EventArgs e)
        {
            GestionePersoneForm guf = new GestionePersoneForm();
            new GestionePersone(guf, Modello, _stato.Utente);
            _forms.Add(guf);
            guf.Show();
        }

        void Salva_Click(object sender, EventArgs e)
        {
            Modello.Salva();
        }

        private void ListChanged(object sender, ListChangedEventArgs e)
        {
            _stato.ListChanged(sender, e);
        }

        /// <summary>
        /// Classe che gestisce lo stato dell'autenticazione, mosrando e nascondendo mainView e loginView
        /// </summary>
        private abstract class StatoUtente
        {
            public Persona Utente { get; private set; }
            public IMainView Main { get; private set; }
            public ILoginView Login { get; private set; }

            protected StatoUtente(Persona utente, IMainView mv, ILoginView lv)
            {
                if (mv == null || lv == null)
                    throw new ArgumentNullException();

                Utente = utente;
                Main = mv;
                Login = lv;
            }

            public abstract void ListChanged(object sender, ListChangedEventArgs e);
        }

        private class Sconosciuto : StatoUtente
        {
            public Sconosciuto(IMainView mv, ILoginView lv) : base(null, mv, lv)
            {
                mv.Hide();
                lv.Show();
            }

            public override void ListChanged(object sender, ListChangedEventArgs e)
            {
            }
        }

        private class Autenticato : StatoUtente
        {
            protected Func<IEnumerable<Turno>> _turni;

            public Autenticato(Persona utente, IMainView mv, ILoginView lv, Func<IEnumerable<Turno>> turni) : base(utente, mv, lv)
            {
                if (utente == null)
                    throw new ArgumentException("utente nullo");
                if (turni == null)
                    throw new ArgumentException("utente");

                _turni = turni;

                lv.Hide();
                mv.Gestione.Visible = utente.IsAmministratore;
                ListChanged(null, null);
                mv.Show();
            }

            public override void ListChanged(object sender, ListChangedEventArgs e)
            {
                SortedSet<DettagliTurno> dettagli = new SortedSet<DettagliTurno>();

                foreach (Turno t in _turni())
                {
                    if (t.Equipaggio.Contains(Utente))
                    {
                        string camion = t.CamionUsato.ToString(), rifiuto = t.Tipo.ToString(), destinazione = t.PercorsoEffuttuato.Dest.ToString();
                        DateTime inizio, fine = DateTime.Now;
                        while (t.ProssimoInizio(fine, out inizio))
                            if (t.ProssimaFine(inizio, out fine))
                                dettagli.Add(new DettagliTurno(inizio, fine, camion, string.Join(Environment.NewLine, from p in t.PercorsoEffuttuato.Itinerario select p.Nome), destinazione, string.Join(Environment.NewLine, from p in t.Equipaggio select p.CodiceFiscale), rifiuto));
                    }
                }

                Main.Calendario.DataSource = new BindingList<DettagliTurno>(dettagli.ToList());

                if (e != null)
                {
                    if (e.PropertyDescriptor.Name == "IsAmministratore")
                    {
                        foreach (ToolStripMenuItem i in Main.Gestione.DropDownItems)
                            if (i != Main.Salva && i != Main.SalvaSu)
                                i.Enabled = false;
                    }
                }
            }
        }
    }
}
