﻿using RaccoltaRifiuti.Model;
using RaccoltaRifiuti.Persistenza;
using RaccoltaRifiuti.Utils;
using RaccoltaRifiuti.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.Presenter.WindowsForms
{
    class LoginPresenter
    {
        private ILoginView _view;
        private ICredenziali _credenziali;
        private Action<Persona> _callback;
        private Func<IEnumerable<Persona>> _persone;

        public LoginPresenter(Action<Persona> loginCallback, ILoginView view, ICredenziali credenziali, Func<IEnumerable<Persona>> persone)
        {
            if (loginCallback == null)
                throw new ArgumentNullException("parent");
            if (view == null)
                throw new ArgumentNullException("view");
            if (credenziali == null)
                throw new ArgumentNullException("credenziali");
            if (persone == null)
                throw new ArgumentNullException("persone");

            _view = view;
            _credenziali = credenziali;
            _callback = loginCallback;
            _persone = persone;

            // Aggancia gli handler degli input
            view.CodiceFiscale.TextChanged += TextChanged;
            view.Password.TextChanged += TextChanged;
            view.Login.Click += Login_Click;

            // Alla chiusura del form esci dall'applicazione
            view.FormClosed += ((s, e) => Application.Exit());
        }

        private void Login_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_credenziali.VerificaCredenziali(_view.CodiceFiscale.Text, _view.Password.Text))
                    MessageBox.Show("Codice fiscale o password invalido", "Login fallito", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    Persona utente = _persone().First(p => p.CodiceFiscale == _view.CodiceFiscale.Text.Ripulisci());
                    _callback(utente);
                    _view.CodiceFiscale.Text = "";
                    _view.Password.Text = "";
                }
            }
            catch (InvalidOperationException)
            {
                MessageBox.Show("Codice fiscale non trovato nei dati", "Login fallito", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Login fallito", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void TextChanged(object sender, EventArgs e)
        {
            _view.Login.Enabled = !string.IsNullOrWhiteSpace(_view.CodiceFiscale.Text) && _view.Password.TextLength != 0;
        }
    }
}
