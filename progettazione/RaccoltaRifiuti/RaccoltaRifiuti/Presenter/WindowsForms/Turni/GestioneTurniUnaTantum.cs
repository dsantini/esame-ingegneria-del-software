﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RaccoltaRifiuti.View;
using RaccoltaRifiuti.View.WindowsForms;
using RaccoltaRifiuti.Model;
using RaccoltaRifiuti.Model.Turni;
using System.Windows.Forms;

namespace RaccoltaRifiuti.Presenter.WindowsForms.Turni
{
    class GestioneTurniUnaTantum : GestioneTurni
    {
        public static string Titolo { get { return "Turni una tantum"; } }

        private DateTimePicker _dtp;

        public GestioneTurniUnaTantum(ITurniView gest, ITurni mod) : base(gest, mod)
        {
            // Prepara il controllo
            TurnoUnaTantumControl tutc = new TurnoUnaTantumControl();
            View.PanelTurno.Controls.Add(tutc);
            _dtp = tutc.Inizio;
            _dtp.Value = DateTime.Now;

            // Aggancia gli handler degli input
            tutc.Inizio.ValueChanged += SelectedIndexChanged;

            // Prepara la DataGridView
            mod.Turni.ListChanged += SincronizzaLista;
            SincronizzaLista(null, null);
        }

        protected override void SincronizzaLista(object sender, ListChangedEventArgs e)
        {
            View.GridTurno.DataSource = new BindingList<TurnoUnaTantum>(Mod.Turni.OfType<TurnoUnaTantum>().ToList());
        }

        protected override void Aggiungi_Click(object sender, EventArgs e)
        {
            try
            {
                Mod.AggiungiTurno(new TurnoUnaTantum(_dtp.Value, Mod.NuovoIdTurno(), (Percorso)View.Percorso.SelectedItem, Enumerable.ToList<Persona>(View.Equipaggio.CheckedItems.OfType<Persona>()), (Camion)View.Camion.SelectedItem, (TipoRifiuto)View.Tipo.SelectedItem));
            }
            catch (Exception exc)
            {
                MessageBox.Show("Parametri non validi: " + exc.Message, "Errore nell'inserimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void SelectedIndexChanged(object sender, EventArgs e)
        {
            View.Aggiungi.Enabled = SelectedIndexChangedHelper();
        }
    }
}
