﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RaccoltaRifiuti.Model;
using System.ComponentModel;

namespace RaccoltaRifiuti.Presenter
{
    public interface ICamions
    {
        BindingList<Camion> Camion { get; }
        BindingList<TipoRifiuto> TipiDiRifiuto { get; }

        void AggiungiCamion(string targa, ICollection<TipoRifiuto> tipi);
        void AggiungiCamion(Camion c);
    }
}
