﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RaccoltaRifiuti.Persistenza;
using RaccoltaRifiuti.Model;
using System.ComponentModel;

namespace RaccoltaRifiuti.Presenter.Dati
{
    public class GestioneModello : IModello
    {
        private IPersistenza _persistenza;

        public BindingList<Destinazione> Destinazioni { get; private set; }
        public BindingList<PuntoDiRaccolta> PuntiDiRaccolta { get; private set; }
        public BindingList<Persona> Persone { get; private set; }
        public BindingList<TipoRifiuto> TipiDiRifiuto { get; private set; }
        public BindingList<Camion> Camion { get; private set; }
        public BindingList<Percorso> Percorsi { get; private set; }
        public BindingList<Turno> Turni { get; private set; }

        public IEnumerable<string> NomiTipiDiRifiuto { get { return from tipo in TipiDiRifiuto select tipo.Nome; } }
        public IEnumerable<string> CodiciFiscaliPersone { get { return from persona in Persone select persona.CodiceFiscale; } }
        public IEnumerable<string> NomiPuntiDiRaccolta { get { return from punto in PuntiDiRaccolta select punto.Nome; } }
        private static readonly TimeSpan _unSecondo = new TimeSpan(0, 0, 1);

        public GestioneModello(IPersistenza persistenza)
        {
            if (persistenza == null)
                throw new ArgumentNullException("persistenza");

            _persistenza = persistenza;

            Destinazioni = new BindingList<Destinazione>();
            foreach (Destinazione d in persistenza.CaricaDestinazioni())
                try { AggiungiDestinazione(d); } catch (Exception e) { Console.WriteLine("Errore nel caricamento da persistenza: " + e.Message); }
            Destinazioni.ListChanged += Destinazioni_ListChanged;

            PuntiDiRaccolta = new BindingList<PuntoDiRaccolta>();
            foreach (PuntoDiRaccolta p in persistenza.CaricaPuntiDiRaccolta())
                try { AggiungiPuntoDiRaccolta(p); } catch (Exception e) { Console.WriteLine("Errore nel caricamento da persistenza: " + e.Message); }
            PuntiDiRaccolta.ListChanged += PuntiDiRaccolta_ListChanged;

            Persone = new BindingList<Persona>();
            foreach (Persona p in persistenza.CaricaPersone())
                try { AggiungiPersona(p); } catch (Exception e) { Console.WriteLine("Errore nel caricamento da persistenza: " + e.Message); }
            Persone.ListChanged += Persone_ListChanged;

            TipiDiRifiuto = new BindingList<TipoRifiuto>();
            foreach (TipoRifiuto t in persistenza.CaricaTipiDiRifiuto())
                try { AggiungiTipoDiRifiuto(t); } catch (Exception e) { Console.WriteLine("Errore nel caricamento da persistenza: " + e.Message); }
            TipiDiRifiuto.ListChanged += TipiDiRifiuto_ListChanged;

            Camion = new BindingList<Camion>();
            foreach (Camion c in persistenza.CaricaCamion(TipiDiRifiuto))
                try { AggiungiCamion(c); } catch (Exception e) { Console.WriteLine("Errore nel caricamento da persistenza: " + e.Message); }
            Camion.ListChanged += Camion_ListChanged;

            Percorsi = new BindingList<Percorso>();
            foreach (Percorso v in persistenza.CaricaPercorsi(Destinazioni, PuntiDiRaccolta))
                try { AggiungiPercorso(v); } catch (Exception e) { Console.WriteLine("Errore nel caricamento da persistenza: " + e.Message); }
            Percorsi.ListChanged += Percorsi_ListChanged;

            Turni = new BindingList<Turno>();
            foreach (Turno t in persistenza.CaricaTurni(Persone, TipiDiRifiuto, Camion, Percorsi))
                try { AggiungiTurno(t); } catch (Exception e) { Console.WriteLine("Errore nel caricamento da persistenza: " + e.Message); }

        }

        private void Percorsi_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.ItemDeleted)
                // Percorso eliminato, rimuovi turni che dipendono da esso
                for (int i = Turni.Count - 1; i >= 0; i--)
                    if (!Percorsi.Contains(Turni[i].PercorsoEffuttuato))
                        Turni.RemoveAt(i);
        }

        private void Camion_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.ItemDeleted)
                // Camion eliminato, rimuovi turni che dipendono da esso
                for (int i = Turni.Count - 1; i >= 0; i--)
                    if (!Camion.Contains(Turni[i].CamionUsato))
                        Turni.RemoveAt(i);
        }

        private void TipiDiRifiuto_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.ItemDeleted)
            {
                // Tipo di rifiuto eliminato, rimuovi turni che dipendono da esso
                for (int i = Turni.Count - 1; i >= 0; i--)
                    if (!TipiDiRifiuto.Contains(Turni[i].Tipo))
                        Turni.RemoveAt(i);

                // Tipo di rifuto eliminato, rimuovi i camion che dipendono da esso
                for (int i = Camion.Count - 1; i >= 0; i--)
                {
                    Camion c = Camion[i];
                    for (int j = c.TipiSupportati.Count() - 1; j >= 0; j--)
                    {
                        TipoRifiuto t = c.TipiSupportati.ElementAt(j);
                        if (!TipiDiRifiuto.Contains(t))
                            c.TipiSupportati.Remove(t);
                    }

                    if (c.TipiSupportati.Count == 0)
                        Camion.RemoveAt(i);
                }
            }
        }

        private void Persone_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.ItemDeleted)
            {
                // Persona eliminata, rimuovi i turni che dipendono da essa
                for (int i = Turni.Count - 1; i >= 0; i--)
                {
                    Turno t = Turni[i];
                    for (int j = t.Equipaggio.Count() - 1; j >= 0; j--)
                    {
                        Persona p = t.Equipaggio.ElementAt(j);
                        if (!Persone.Contains(p))
                            t.Equipaggio.Remove(p);
                    }

                    if (t.Equipaggio.Count == 0)
                        Turni.RemoveAt(i);
                }
            }
        }

        private void PuntiDiRaccolta_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.ItemDeleted)
            {
                // Punto di raccolta eliminato, rimuovi i Percorsi che dipendono da esso
                for (int i = Percorsi.Count - 1; i >= 0; i--)
                {
                    Percorso v = Percorsi[i];
                    for (int j = v.Itinerario.Count() - 1; j >= 0; j--)
                    {
                        PuntoDiRaccolta p = v.Itinerario.ElementAt(j);
                        if (!PuntiDiRaccolta.Contains(p))
                            v.Itinerario.Remove(p);
                    }

                    if (v.Itinerario.Count == 0)
                        Turni.RemoveAt(i);
                }
            }
        }

        private void Destinazioni_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.ItemDeleted)
                // Destinazione eliminata, rimuovi i Percorsi che dipendono da essa
                for (int i = Percorsi.Count - 1; i >= 0; i--)
                    if (!Destinazioni.Contains(Percorsi[i].Dest))
                        Percorsi.RemoveAt(i);
        }

        public void Salva(IPersistenza persistenza)
        {
            persistenza.SalvaTutto(Destinazioni, Persone, PuntiDiRaccolta, TipiDiRifiuto, Camion, Percorsi, Turni);
        }

        public void Salva()
        {
            Salva(_persistenza);
        }

        public void AggiungiDestinazione(string nome, string indirizzo)
        {
            AggiungiDestinazione(new Destinazione(nome, indirizzo));
        }

        public void AggiungiDestinazione(Destinazione d)
        {
            if (d == null)
                throw new ArgumentNullException("Destinazione nulla");

            if (Destinazioni.Any(destinazione => destinazione.Indirizzo.Equals(d.Indirizzo, StringComparison.OrdinalIgnoreCase)))
                throw new ApplicationException("Una destinazione con l'indirizzo " + d.Indirizzo + " esiste già");

            Destinazioni.Add(d);
        }

        public void AggiungiPersona(string codiceFiscale, string nome, string cognome)
        {
            AggiungiPersona(new Persona(nome, cognome, codiceFiscale));
        }

        public void AggiungiPersona(Persona p)
        {
            if (p == null)
                throw new ArgumentNullException("Persona null");

            if (Persone.Any(persona => persona.CodiceFiscale.Equals(p.CodiceFiscale, StringComparison.OrdinalIgnoreCase)))
                throw new ApplicationException("Un utente con il codice fiscale " + p.CodiceFiscale + " esiste già");

            Persone.Add(p);
        }

        public void AggiungiCamion(string targa, ICollection<TipoRifiuto> tipi)
        {
            AggiungiCamion(new Camion(targa, tipi));
        }

        public void AggiungiCamion(Camion c)
        {
            if (c == null)
                throw new ArgumentNullException("Camion nullo");

            if (Camion.Any(camion => camion.Targa.Equals(c.Targa, StringComparison.OrdinalIgnoreCase)))
                throw new ApplicationException("Un camion con la targa " + c.Targa + " esiste già");

            Camion.Add(c);
        }

        public void AggiungiPuntoDiRaccolta(string nome, string indirizzo)
        {
            AggiungiPuntoDiRaccolta(new PuntoDiRaccolta(nome, indirizzo));
        }

        public void AggiungiPuntoDiRaccolta(PuntoDiRaccolta p)
        {
            if (p == null)
                throw new ArgumentNullException("Punto di raccolta nullo");

            if (PuntiDiRaccolta.Any(punto => punto.Nome.Equals(p.Nome, StringComparison.OrdinalIgnoreCase)))
                throw new ApplicationException("Un punto di raccolta con il nome " + p.Nome + " esiste già");

            PuntiDiRaccolta.Add(p);
        }

        public void AggiungiTipoDiRifiuto(string nome)
        {
            AggiungiTipoDiRifiuto(new TipoRifiuto(nome));
        }

        public void AggiungiTipoDiRifiuto(TipoRifiuto t)
        {
            if (t == null)
                throw new ArgumentNullException("Tipo di rifiuto nullo");

            if (TipiDiRifiuto.Any(tipo => tipo.Nome.Equals(t.Nome, StringComparison.OrdinalIgnoreCase)))
                throw new ApplicationException("Un tipo di rifuto con il nome " + t.Nome + " esiste già");

            TipiDiRifiuto.Add(t);
        }

        public int NuovoIdPercorso()
        {
            for (int i = int.MinValue; i < int.MaxValue; i++)
                if (Percorsi.All(Percorso => Percorso.Identificativo != i))
                    return i;

            throw new ApplicationException("Non esistono più ID liberi per Percorsi. Elimina un Percorso.");
        }

        public void AggiungiPercorso(TimeSpan durata, Destinazione dest, ICollection<PuntoDiRaccolta> itinerario)
        {
            AggiungiPercorso(new Percorso(durata, NuovoIdPercorso(), dest, itinerario));
        }

        public void AggiungiPercorso(Percorso v)
        {
            if (v == null)
                throw new ArgumentNullException("Percorso nullo");

            if (Percorsi.Any(Percorso => Percorso.Identificativo == v.Identificativo))
                throw new ApplicationException("Un Percorso con l'identificativo " + v.Identificativo + " esiste già");

            Percorsi.Add(v);
        }

        public int NuovoIdTurno()
        {
            for (int i = int.MinValue; i < int.MaxValue; i++)
                if (Turni.All(turno => turno.ID != i))
                    return i;

            throw new ApplicationException("Non esistono più ID liberi per turni. Elimina un turno.");
        }

        public void AggiungiTurno(Turno t)
        {
            if (t == null)
                throw new ArgumentNullException("Turno nullo");

            if (Turni.Any(turno => turno.ID == t.ID))
                throw new ApplicationException("Un turno con l'identificativo " + t.ID + " esiste già");

            // Gli orari del turno non si devono sovrapporre con gli orari di un altro turno con oggetti in comune
            foreach (Turno vecchio in Turni)
            {
                if (t.CamionUsato == vecchio.CamionUsato || vecchio.Equipaggio.Any(membro => t.Equipaggio.Contains(membro)))
                {
                    DateTime momento = DateTime.MinValue, DTNuovo, DTVecchio;
                    // Se entrambi iniziano in DateTime.MinValue lancia una eccezione
                    if (t.InCorso(momento) && vecchio.InCorso(momento))
                        throw new ApplicationException("Il turno si sovrappone in " + momento + " al turno " + vecchio);

                    // Cicla finchè entrambe inizieranno un nuovo turno
                    while (t.ProssimoInizio(momento, out DTNuovo) && vecchio.ProssimoInizio(momento, out DTVecchio))
                    {
                        // Se un turno è già in corso spostati sul primo che inizia, altrimenti spostati sul secondo
                        momento = (t.InCorso(momento) || vecchio.InCorso(momento)) && (DTNuovo > DTVecchio) ? DTVecchio : DTNuovo;

                        // Se nel momento in cui inizia un turno l'altro è ancora in corso, lancia un'eccezione
                        if (t.InCorso(momento) && vecchio.InCorso(momento))
                            throw new ApplicationException("Il turno si sovrappone in " + momento + " al turno " + vecchio);

                        // Se entrambi finiranno un turno
                        if (t.ProssimaFine(momento, out DTNuovo) && vecchio.ProssimaFine(momento, out DTVecchio))
                        {
                            // Spostati un secondo prima della fine del turno che finisce prima
                            momento = (DTNuovo > DTVecchio ? DTVecchio : DTNuovo) - _unSecondo;

                            // Se nel momento in cui inizia un turno l'altro è ancora in corso, lancia un'eccezione
                            if (t.InCorso(momento) && vecchio.InCorso(momento))
                                throw new ApplicationException("Il turno si sovrappone in " + momento + " al turno " + vecchio);
                        }
                    }
                }
            }

            Turni.Add(t);
        }
    }
}
