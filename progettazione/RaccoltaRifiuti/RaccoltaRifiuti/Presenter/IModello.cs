﻿using RaccoltaRifiuti.Persistenza;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaccoltaRifiuti.Presenter
{
    public interface IModello : ICamions, IDestinazioni, IPersone, IPuntiDiRaccolta, ITipiDiRifiuto, ITurni, IPercorsi
    {
        void Salva(IPersistenza persistenza);
        void Salva();
    }
}
