﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaccoltaRifiuti.Model
{
    public class PuntoDiRaccolta
    {
        public string Nome { get; private set; } // primary key
        public string Via { get; private set; }

        public PuntoDiRaccolta(string nome, string via) {
            if (String.IsNullOrWhiteSpace(nome))
                throw new ArgumentException("Nome non valido");

            if (String.IsNullOrWhiteSpace(via))
                throw new ArgumentException("Indirizzo non valido");


            Nome = nome;
            Via = via;
        }

        public override string ToString()
        {
            return Via + "(" + Nome + ")";
        }
    }
}
