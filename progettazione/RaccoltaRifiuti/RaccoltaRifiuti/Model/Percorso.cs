﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace RaccoltaRifiuti.Model
{
    public class Percorso
    {
        public TimeSpan DurataPrevista { get; private set; }
        public int Identificativo { get; private set; } // primary key
        public Destinazione Dest { get; private set; }
        public ICollection<PuntoDiRaccolta> Itinerario { get; private set; }

        public Percorso(TimeSpan durataPrevista, int id, Destinazione dest, ICollection<PuntoDiRaccolta> itinerario)
        {
            if (durataPrevista <= TimeSpan.Zero)
                throw new ArgumentException("Durata invalida");

            if (dest == null)
                throw new ArgumentNullException("Destinazione invalida");

            if (itinerario == null)
                throw new ArgumentNullException("Itinerario nullo");
            if (itinerario.Count() == 0)
                throw new ArgumentException("Itinerario vuoto");
            if (itinerario.Contains(null))
                throw new ArgumentException("Itinerario contiene null");

            DurataPrevista = durataPrevista;
            Identificativo = id;
            Dest = dest;
            Itinerario = itinerario;
        }

        public override string ToString()
        {
            string res = Itinerario.ElementAt(0).Nome;

            if (Itinerario.Count() > 1)
                res += " > " + Itinerario.ElementAt(Itinerario.Count() - 1).Nome;

            return res + " > " + Dest;
        }
    }
}
