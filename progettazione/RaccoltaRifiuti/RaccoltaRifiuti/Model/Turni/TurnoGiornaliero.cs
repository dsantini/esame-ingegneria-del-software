﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaccoltaRifiuti.Model.Turni
{
    public class TurnoGiornaliero : TurnoIntervalloDateOrarioFisso
    {
        private DateTime _ultimoInizio, _ultimaFine;
        private static readonly TimeSpan unGiorno = TimeSpan.FromDays(1);

        public TurnoGiornaliero(DateTime oraInizio, DateTime dataInizio, DateTime dataFine, int id, Percorso Percorso, ICollection<Persona> equipaggio, Camion camion, TipoRifiuto tipo)
            : base(oraInizio.TimeOfDay, dataInizio, dataFine, id, Percorso, equipaggio, camion, tipo)
        {
            if (Percorso.DurataPrevista >= unGiorno)
                throw new ArgumentException("La durata prevista del Percorso è troppo lunga per un turno giornaliero");
            
            try
            {
                _ultimoInizio = DataDiFine + OrarioDiInizio;
                _ultimaFine = DataDiFine + OrarioFine;
            }
            catch (ArgumentOutOfRangeException) {
                throw new ArgumentException("Hai inserito una data di fine troppo alta");
            }
        }

        public override bool InCorso(DateTime momento)
        {
            if (momento >= _ultimaFine || momento < PrimoInizio) // Fuori range
                return false;

            DateTime inizio = momento.Date + OrarioDiInizio,
                fine = inizio + PercorsoEffuttuato.DurataPrevista;
            if (momento >= inizio && momento < fine) // E' in corso il turno odierno
                return true;

            DateTime inizoPrecedente = inizio - unGiorno,
                finePrecedente = fine - unGiorno;
            return momento >= inizoPrecedente && momento < finePrecedente; // E' in corso il turno iniziato ieri
        }

        public override bool ProssimoInizio(DateTime momento, out DateTime prossimo)
        {
            if (momento < PrimoInizio) // Fuori dal range
            {
                prossimo = PrimoInizio;
                return true;
            }

			if (momento >= _ultimoInizio) // Fuori dal range
            {
                prossimo = DateTime.MinValue;
                return false;
            }

            DateTime inizioDiGiornata = momento.Date + OrarioDiInizio;
            if (momento < inizioDiGiornata) // Prima dell'inizio giornaliero
            {
                prossimo = inizioDiGiornata;
                return true;
            }

            // Dopo l'inizio giornaliero
            prossimo = inizioDiGiornata + unGiorno;
            return true;
        }

        public override bool ProssimaFine(DateTime momento, out DateTime prossimo)
        {
            if (momento < PrimaFine) // Fuori dal range
            {
                prossimo = PrimaFine;
                return true;
            }

			if (momento >= _ultimaFine) // Fuori dal range
            {
                prossimo = DateTime.MaxValue;
                return false;
            }

            DateTime fineDiGiornata = momento.Date + OrarioFine,
                fineGiornataPrecedente = fineDiGiornata - unGiorno;
            if (momento < fineGiornataPrecedente)
            {
                prossimo = fineGiornataPrecedente;
                return true;
            }

            if (momento < fineDiGiornata)
            {
                prossimo = fineDiGiornata;
                return true;
            }

            prossimo = fineDiGiornata + unGiorno;
            return true;
        }

        public override string ToString()
        {
            return base.ToString() + "; TurnoGiornaliero";
        }
    }
}
