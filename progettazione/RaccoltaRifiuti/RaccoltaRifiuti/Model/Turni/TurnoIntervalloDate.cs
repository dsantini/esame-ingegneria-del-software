﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaccoltaRifiuti.Model.Turni
{
    public abstract class TurnoIntervalloDate : Turno
    {
        public DateTime DataDiInizio { get; private set; } // Inclusiva
        public DateTime DataDiFine { get; private set; } // Inclusiva

        public TurnoIntervalloDate(DateTime dataInizio, DateTime dataFine, int id, Percorso percorso, ICollection<Persona> equipaggio, Camion camion, TipoRifiuto tipo)
            : base(id, percorso, equipaggio, camion, tipo)
        {
            if (dataInizio.CompareTo(dataFine) > 0)
                throw new ArgumentException("La data di fine precede la data di inizio");

            DataDiFine = dataFine.Date;
            DataDiInizio = dataInizio.Date;
        }

        public override string ToString()
        {
            return base.ToString() + "; Intervallo date: " + DataDiInizio + " => " + DataDiFine;
        }
    }
}
