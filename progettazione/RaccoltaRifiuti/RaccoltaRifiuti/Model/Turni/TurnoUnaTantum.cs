﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaccoltaRifiuti.Model.Turni
{
    class TurnoUnaTantum : Turno
    {
        public DateTime Inizio { get; private set; }

        private DateTime _fine;

        public TurnoUnaTantum(DateTime dataOraInizio, int id, Percorso Percorso, ICollection<Persona> equipaggio, Camion camion, TipoRifiuto tipo) : base(id, Percorso, equipaggio, camion, tipo)
        {
            Inizio = dataOraInizio;
            _fine = Inizio + PercorsoEffuttuato.DurataPrevista;
        }

        public override bool InCorso(DateTime momento)
        {
            return momento >= Inizio && momento < _fine;
        }

        public override bool ProssimaFine(DateTime momento, out DateTime prossimo)
        {
            prossimo = _fine;
            return momento < _fine;
        }

        public override bool ProssimoInizio(DateTime momento, out DateTime prossimo)
        {
            prossimo = Inizio;
            return momento < Inizio;
        }

        public override string ToString()
        {
            return base.ToString() + "; Una tantum: " + Inizio;
        }
    }
}
