﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaccoltaRifiuti.Model.Turni
{
    public abstract class TurnoIntervalloDateOrarioFisso : TurnoIntervalloDate
    {
        public TimeSpan OrarioDiInizio { get; private set; }

        protected TimeSpan OrarioFine { get; private set; }
        protected DateTime PrimoInizio { get; private set; }
        protected DateTime PrimaFine { get; private set; }

        public TurnoIntervalloDateOrarioFisso(TimeSpan oraInizio, DateTime dataInizio, DateTime dataFine, int id, Percorso percorso, ICollection<Persona> equipaggio, Camion camion, TipoRifiuto tipo)
            : base(dataInizio, dataFine, id, percorso, equipaggio, camion, tipo)
        {
            if (oraInizio > TimeSpan.FromDays(1))
                throw new ArgumentException("Orario mmaggiore della durata di una giornata");

            OrarioDiInizio = oraInizio;

            OrarioFine = OrarioDiInizio + PercorsoEffuttuato.DurataPrevista;
            PrimoInizio = DataDiInizio + OrarioDiInizio;
            PrimaFine = DataDiInizio + OrarioFine;
        }

        public override string ToString()
        {
            return base.ToString() + "; Ora: " + OrarioDiInizio;
        }
    }
}
