﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.View
{
    public interface ITipiDiRifiutoView
    {
        DataGridView TipiView { get; }
        TextBox Nome { get; }
        Button Aggiungi { get; }
        Button Rimuovi { get; }
    }
}
