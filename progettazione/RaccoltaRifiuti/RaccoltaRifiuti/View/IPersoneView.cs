﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.View
{
    public interface IPersoneView
    {
        Button AggiungiUtente { get; }
        Button RimuoviUtente { get; }
        TextBox NomeUtente { get; }
        TextBox CognomeUtente { get; }
        TextBox CodiceFiscaleUtente { get; }
        DataGridView Utenti { get; }
    }
}
