﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.View
{
    public interface ICredenzialiView
    {
        ListControl Utente { get; }
        TextBox Password { get; }
        Control Conferma { get; }
        Control Rimuovi { get; }
    }
}
