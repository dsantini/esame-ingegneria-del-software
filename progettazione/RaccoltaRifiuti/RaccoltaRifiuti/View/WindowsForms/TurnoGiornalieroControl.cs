﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.View.WindowsForms
{
    public partial class TurnoGiornalieroControl : UserControl, IIntervalloDateOrarioView
    {

        public DateTimePicker DataInizio { get { return _dataInizio; } }
        public DateTimePicker OraInizio { get { return _oraInizio; } }
        public DateTimePicker DataFine { get { return _dataFine; } }

        public TurnoGiornalieroControl()
        {
            InitializeComponent();
        }


    }
}
