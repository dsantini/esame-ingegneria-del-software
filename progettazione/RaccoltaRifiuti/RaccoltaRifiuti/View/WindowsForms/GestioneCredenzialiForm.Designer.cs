﻿namespace RaccoltaRifiuti.View.WindowsForms
{
    partial class GestioneCredenzialiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._utente = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._password = new System.Windows.Forms.TextBox();
            this._applica = new System.Windows.Forms.Button();
            this._rimuovi = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _utente
            // 
            this._utente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._utente.FormattingEnabled = true;
            this._utente.Location = new System.Drawing.Point(71, 6);
            this._utente.Name = "_utente";
            this._utente.Size = new System.Drawing.Size(251, 21);
            this._utente.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Utente";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Password";
            // 
            // _password
            // 
            this._password.Location = new System.Drawing.Point(71, 31);
            this._password.Name = "_password";
            this._password.PasswordChar = '*';
            this._password.Size = new System.Drawing.Size(251, 20);
            this._password.TabIndex = 3;
            // 
            // _applica
            // 
            this._applica.Enabled = false;
            this._applica.Location = new System.Drawing.Point(71, 58);
            this._applica.Name = "_applica";
            this._applica.Size = new System.Drawing.Size(75, 23);
            this._applica.TabIndex = 4;
            this._applica.Text = "Applica";
            this._applica.UseVisualStyleBackColor = true;
            // 
            // _rimuovi
            // 
            this._rimuovi.Enabled = false;
            this._rimuovi.Location = new System.Drawing.Point(247, 58);
            this._rimuovi.Name = "_rimuovi";
            this._rimuovi.Size = new System.Drawing.Size(75, 23);
            this._rimuovi.TabIndex = 5;
            this._rimuovi.Text = "Rimuovi";
            this._rimuovi.UseVisualStyleBackColor = true;
            // 
            // GestioneCredenzialiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 95);
            this.Controls.Add(this._rimuovi);
            this.Controls.Add(this._applica);
            this.Controls.Add(this._password);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._utente);
            this.Name = "GestioneCredenzialiForm";
            this.Text = "Gestione Credenziali";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox _utente;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _password;
        private System.Windows.Forms.Button _applica;
        private System.Windows.Forms.Button _rimuovi;
    }
}