﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.View.WindowsForms
{
    public partial class GestionePercorsiForm : Form, IPercorsiView
    {

        public DataGridView DataView { get { return _dataGridView; } }
        public ComboBox Destinazione { get { return _comboBox; } }
        public CheckedListBox PuntiDiRaccolta { get { return _checkedListBox; } }
        public Button Aggiungi { get { return _aggiungi; } }
        public Button Rimuovi { get { return _rimuovi; } }
        public DateTimePicker DurataPrevista { get { return _durataPrevista; } }
        public GestionePercorsiForm()
        {
            InitializeComponent();
        }


    }
}
