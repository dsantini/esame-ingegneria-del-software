﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.View.WindowsForms
{
    public partial class GestioneCredenzialiForm : Form, ICredenzialiView
    {
        public GestioneCredenzialiForm()
        {
            InitializeComponent();
        }

        public ListControl Utente { get { return _utente; } }
        public TextBox Password { get { return _password; } }
        public Control Conferma { get { return _applica; } }
        public Control Rimuovi { get { return _rimuovi; } }
    }
}
