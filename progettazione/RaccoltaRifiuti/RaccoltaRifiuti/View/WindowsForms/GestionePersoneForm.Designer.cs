﻿namespace RaccoltaRifiuti.View.WindowsForms
{
    partial class GestionePersoneForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this._rimuoviUtenteButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._aggiungiUtenteButton = new System.Windows.Forms.Button();
            this._cfUtenteBox = new System.Windows.Forms.TextBox();
            this._cognomeUtenteBox = new System.Windows.Forms.TextBox();
            this._nomeUtenteBox = new System.Windows.Forms.TextBox();
            this._utentiView = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._utentiView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this._rimuoviUtenteButton);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this._aggiungiUtenteButton);
            this.panel1.Controls.Add(this._cfUtenteBox);
            this.panel1.Controls.Add(this._cognomeUtenteBox);
            this.panel1.Controls.Add(this._nomeUtenteBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(897, 29);
            this.panel1.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(422, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Codice fiscale:";
            // 
            // _rimuoviUtenteButton
            // 
            this._rimuoviUtenteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._rimuoviUtenteButton.Enabled = false;
            this._rimuoviUtenteButton.Location = new System.Drawing.Point(819, 3);
            this._rimuoviUtenteButton.Name = "_rimuoviUtenteButton";
            this._rimuoviUtenteButton.Size = new System.Drawing.Size(75, 23);
            this._rimuoviUtenteButton.TabIndex = 7;
            this._rimuoviUtenteButton.Text = "Rimuovi";
            this._rimuoviUtenteButton.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(205, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Cognome:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Nome:";
            // 
            // _aggiungiUtenteButton
            // 
            this._aggiungiUtenteButton.Enabled = false;
            this._aggiungiUtenteButton.Location = new System.Drawing.Point(660, 3);
            this._aggiungiUtenteButton.Name = "_aggiungiUtenteButton";
            this._aggiungiUtenteButton.Size = new System.Drawing.Size(75, 23);
            this._aggiungiUtenteButton.TabIndex = 3;
            this._aggiungiUtenteButton.Text = "Aggiungi";
            this._aggiungiUtenteButton.UseVisualStyleBackColor = true;
            // 
            // _cfUtenteBox
            // 
            this._cfUtenteBox.Location = new System.Drawing.Point(504, 5);
            this._cfUtenteBox.Name = "_cfUtenteBox";
            this._cfUtenteBox.Size = new System.Drawing.Size(150, 20);
            this._cfUtenteBox.TabIndex = 2;
            // 
            // _cognomeUtenteBox
            // 
            this._cognomeUtenteBox.Location = new System.Drawing.Point(266, 5);
            this._cognomeUtenteBox.Name = "_cognomeUtenteBox";
            this._cognomeUtenteBox.Size = new System.Drawing.Size(150, 20);
            this._cognomeUtenteBox.TabIndex = 1;
            // 
            // _nomeUtenteBox
            // 
            this._nomeUtenteBox.AcceptsTab = true;
            this._nomeUtenteBox.AllowDrop = true;
            this._nomeUtenteBox.Location = new System.Drawing.Point(49, 5);
            this._nomeUtenteBox.Name = "_nomeUtenteBox";
            this._nomeUtenteBox.Size = new System.Drawing.Size(150, 20);
            this._nomeUtenteBox.TabIndex = 0;
            // 
            // _utentiView
            // 
            this._utentiView.AllowUserToOrderColumns = true;
            this._utentiView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this._utentiView.BackgroundColor = System.Drawing.SystemColors.Control;
            this._utentiView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._utentiView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._utentiView.Location = new System.Drawing.Point(0, 29);
            this._utentiView.Name = "_utentiView";
            this._utentiView.RowHeadersVisible = false;
            this._utentiView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._utentiView.Size = new System.Drawing.Size(897, 384);
            this._utentiView.TabIndex = 5;
            // 
            // GestionePersoneForm
            // 
            this.AcceptButton = this._aggiungiUtenteButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(897, 413);
            this.Controls.Add(this._utentiView);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(750, 150);
            this.Name = "GestionePersoneForm";
            this.Text = "Gestione Persone";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._utentiView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button _rimuoviUtenteButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _aggiungiUtenteButton;
        private System.Windows.Forms.TextBox _cfUtenteBox;
        private System.Windows.Forms.TextBox _cognomeUtenteBox;
        private System.Windows.Forms.TextBox _nomeUtenteBox;
        private System.Windows.Forms.DataGridView _utentiView;
    }
}

